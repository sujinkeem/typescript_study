const button = document.querySelector("button");
const input1 = document.getElementById("num1")! as HTMLInputElement; // not null & type casting
const input2 = document.getElementById("num2")! as HTMLInputElement;

function add(num1: number, num2: number) { // 파라미터 타입 지정
  return num1 + num2;
}

button.addEventListener("click", function() {
  console.log(add(+input1.value, +input2.value));
});
