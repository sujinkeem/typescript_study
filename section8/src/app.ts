// 데코레이터의 실행은 클래스가 인스턴스화될 때가 아니라 정의될 때 이루어짐
// function Logger(constructor: Function) {
//     console.log('Logging...');
//     console.log(constructor);
// }
// decorator factory function
function Logger(logString: string) {
    console.log('LOGGER FACTORY');
    return function(constructor: Function) {
        console.log(logString);
        console.log(constructor);
    };
}

function WithTemplate(template: string, hookId: string) {
    console.log('TEMPLATE FACTORY');
    return function<T extends {new (...args: any[]): {name: string}}>(
        originalConstructor: T
        ){
        // 새로운 클래스의 생성자 함수를 반환
        return class extends originalConstructor {
            constructor(..._: any[]) { // (_: 사용하지 않음을 알고 무시)
                super();
                console.log('Rendering template');
                const hookEl = document.getElementById(hookId);
                if (hookEl) {
                    hookEl.innerHTML = template;
                    hookEl.querySelector('h1')!.textContent = this.name;
                }
            }
        };
    }
}

// 데코레이터 실행순서: 아래->위 / 함수 입력순서: 위->아래
// 데코레이터 함수의 생성은 팩토리 함수를 특정하는 순서에 따라서 
@Logger('LOGGING-PERSON')
@WithTemplate('<h1>My Person Object</h1>', 'app')
class Person {
    name = 'Sujin';
    
    constructor() {
        console.log('Creating person object...');
    }
}

//const person = new Person();

//
// log1, log4: 반환은 가능하나 반환값 사용하지 않음 
// log2, log3: 반환 가능. 추가된 메소드의 속성에 관한 설명자를 가질 수 있음.
// Property decorator - 반환 X
function Log(target: any, propertyName: string | Symbol) {
    console.log('Property decorator!');
    console.log(target, propertyName);
}

// Accessor decorator - 반환 O
function Log2(target: any, name: string, descriptor: PropertyDescriptor) {
    console.log('Accessor decorator!');
    console.log(target);
    console.log(name);
    console.log(descriptor);
}

// Method decorator - 반환 O
function Log3(
    target: any,
    name: string | Symbol,
    descriptor: PropertyDescriptor) {
        console.log('Method decorator!');
        console.log(target);
        console.log(name);
        console.log(descriptor); 
}

// Parameter decorator - 반환 X
function Log4(target: any, name: string | Symbol, position: number) {
    console.log('Parameter decorator!');
    console.log(target);
    console.log(name);
    console.log(position);
}

// 모든 데코레이터는 모두 클래스 정의 시 실행. (인스턴스화 하지 않아도 실행) 
class Product {
    @Log // 생성자 함수의 일부로 정의할 때 실행
    title: string;
    private _price: number;

    @Log2
    set price(val: number){
        if (val > 0) {
            this._price = val;
        } else {
            throw new Error('Invalid price - should be positive!');
        }
    }

    constructor(t: string, p:number) {
        this.title = t;
        this._price = p;
    }

    @Log3
    getPriceWithTax(@Log4 tax: number) {
        return this._price * (1 + tax);
    }
}

// 
class Printer {
    message = 'This Works!';

    showMessage() {
        console.log(this.message);
    }
}

const p = new Printer();

const button = document.querySelector('button')!;
button.addEventListener('click', p.showMessage.bind(p));