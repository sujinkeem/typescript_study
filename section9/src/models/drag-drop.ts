  // Drag & Drop Interfaces 
  namespace App {
      // export: 파일 외부에서도 인터페이스 가능
      export interface Draggable {
        dragStartHandler(event: DragEvent): void;
        dragEndHandler(event: DragEvent): void;
      }
      
      export interface DragTarget {
        dragOverHandler(event: DragEvent): void;
        dropHandler(event: DragEvent): void;
        dragLeaveHandler(event: DragEvent): void;
      }
  }
  
