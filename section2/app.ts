// unknown type
let userInput: unknown;
//let userInput: String | number; // 타입을 아는 상황에선 union을 쓰는 게 낫다.
let userName: string;

userInput = 5;
userInput = 'Max';
if (typeof userInput === 'string') {
    userName = userInput;
}

// never type
function generateError (message: string, code: number): never {
    throw {message: message, errorCode: code};
}

generateError('An error occurred!', 500);