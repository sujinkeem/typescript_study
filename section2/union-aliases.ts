// type alias
type Combinable = number | String;
type ConversionDescriptor = 'as-number' | 'as-text';

function combine (
    input1: Combinable, // union type
    input2: Combinable, // union type
    resultConversion: ConversionDescriptor // literal + union type
) {
    let result;
    if (typeof input1 === 'number' && typeof input2 === 'number' || resultConversion === 'as-number') {
        result = +input1 + +input2;
    } else {
        result = input1.toString() + input2.toString();
    }
    return result;
}

const combinedAges = combine(30, 26, 'as-number');
console.log(combinedAges);

const combinedStringAges = combine('30', '26', 'as-number');
console.log(combinedStringAges);

const combinedNames = combine('Max', 'Anna', 'as-text');
console.log(combinedNames);