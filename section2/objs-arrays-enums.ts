// key: type 명시
// const person: {
//   name: string;
//   age: number;
//   hobbies: string[];
//   role: [number, string]; // tuple type: 타입과 길이를 모두 제한
// } = {
// //const person = {
//   name: 'Maximilian',
//   age: 30,
//   hobbies: ['Sports', 'Cooking'],
//   role: [2, 'author']
// };

//person.role.push('admin');
//person.role[1] = 10; // ERROR

// 일일히 지정하지 않아도 됨
//const ADMIN = 0;
//const READ_ONLY = 1;
//const AUTHOR = 2;

// enum: 사용자 지정 유형
enum Role { ADMIN, READ_ONLY, AUTHOR };
//enum Role { ADMIN = 'ADMIN', READ_ONLY = 100, AUTHOR = 200}; // 0에서 시작하여 1씩 증가하는 게 기본값이지만 자유롭게 커스텀 가능 

const person = {
  name: 'Maximilian',
  age: 30,
  hobbies: ['Sports', 'Cooking'],
  role: Role.ADMIN
};

// 배열도 타입 명시
let favoriteActivities: string[];
favoriteActivities = ['Sports'];

console.log(person.name);

for (const hobby of person.hobbies) {
  console.log(hobby.toUpperCase());
  //console.log(hobby.map()); // ERROR
}

if (person.role == Role.ADMIN) {
  console.log('is admin');
}
