function add (n1: number, n2: number) {
    return n1 + n2;
}

// return type 지정
function printResult (num: number): void {
    console.log('Result: ' + num);
}

// callback function 
function addAndHandle (n1: number, n2: number, cb: (num: number) => void) {
    const result = n1 + n2;
    cb(result);
}

printResult(add(5, 12));

// void & undefined 차이
// return 하는 value가 없을 땐 void가 대부분 경우에 쓰이는 기준 

// function type 
//let combineValues: Function;
let combineValues: (a: number, b: number) => number;

combineValues = add; 
console.log(combineValues(5, 6));

addAndHandle(10, 20, (result) => {
    console.log(result);
    // return result; // 콜백 함수가 void 타입일 때도 값을 반환할 수 있음
});
