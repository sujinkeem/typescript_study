/**
 * 타입을 잘못 사용할 때 오류를 알리는 것이 typescript의 핵심 작업
 */
function add(n1: number, n2:number, showResult: boolean, phrase: string) { // type casting
    // 타입 확인하는 코드 불필요
    // if (typeof n1 !== 'number' || typeof n2 !== 'number') {
    //     throw new Error('Incorrect input');
    // }

    const result = n1 + n2;
    if (showResult) {
        console.log(phrase + result);
    } else {
        return result;
    }
}

// 초기값을 할당하지 않으면 변수 선언시 타입을 명시해주는 게 좋다.
// let number1: number;
// number1 = 5;
const number1 = 5;
const number2 = 2.8;
const printResult = true;
const resultPhrase = 'Result is: ';

add(number1, number2, printResult, resultPhrase);