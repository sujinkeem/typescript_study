// Generic Type
// 배열은 어떤 데이터를 내부에 저장해야 하는지, promise는 어떤 데이터를 반환해야 하는지 정의할 수 있음
const names: Array<string> = []; // string[]
//names[0].split(' ');

const promise: Promise<string> = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('');
    }, 2000);
    reject();
});

promise.then(data => {
    data.split(' ');
})

// 사용자 지정 제네릭
function merge<T extends object, U extends object>(objA: T, objB: U) {
    return Object.assign(objA, objB);
}

const mergeObj = merge({name: 'Sujin', hobbies: ['Sports']}, {age: 29});
console.log(mergeObj);

// generic function
interface Lengthy {
    length: number;
}

function countAndDescribe<T extends Lengthy>(element: T): [T, string] {
    let descriptionText = 'Got no value.';
    if (element.length === 1) {
        descriptionText = 'Got 1 element';
    } else if (element.length > 1) {
        descriptionText = 'Got ' + element.length + ' elements.';
    }
    return [element, descriptionText];
}

console.log(countAndDescribe('Hi there!'));
console.log(countAndDescribe([]));

// 
function extractAndConvert<T extends object, U extends keyof T>(
    obj: T,
    key: U
) {
    return 'Value: ' + obj[key];
}

extractAndConvert({name: 'Sujin'}, 'name');

// generic class
class DataStorage<T extends string | number | boolean> {
    private data: T[] = [];

    addItem(item: T) {
        this.data.push(item);
    }

    removeItem(item: T) {
        if (this.data.indexOf(item) === -1) {
            return;
        }
        this.data.splice(this.data.indexOf(item), 1);
    }

    getItems() {
        return [...this.data];
    }
}

const textStorage = new DataStorage<string>();
textStorage.addItem('Max');
textStorage.addItem('Manu');
textStorage.removeItem('Max');
console.log(textStorage.getItems());

const numberStorage = new DataStorage<number>();

// const objectStorage = new DataStorage<object>();
// const maxObj = {name: 'Max'};
// objectStorage.addItem(maxObj);
// objectStorage.addItem({name: 'Manu'});
// // ... 
// objectStorage.removeItem(maxObj);
// console.log(objectStorage.getItems());

// partial type 
interface CourseGoal {
    title: string;
    description: string;
    completeUntil: Date;
}

function createCourseGoal(
    title: string,
    description: string,
    completeUntil: Date
): CourseGoal {
    let courseGoal: Partial<CourseGoal> = {};
    courseGoal.title = title;
    courseGoal.description = description;
    courseGoal.completeUntil = completeUntil;
    return courseGoal as CourseGoal; // partial은 타입이 명확하지 않기 때문에 캐스팅을 해줘야 함
}

// readonly type 
const nameArr: Readonly<String[]> = ['Max', 'Anna'];
//nameArr.push(''); // ERROR
//nameArr.pop(); // ERROR