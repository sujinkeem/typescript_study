/**
 * const: 상수
 * var: 변수 / block scope를 벗어나도 유효
 * let: 변수 / block scope 내에서만 유효
 */

// const add = (a: number, b: number = 1) => a + b;
// const printOutput: (a: number | string) => void = output => console.log(output);

// const button = document.querySelector('button');
// if (button) {
//     button.addeventListener('click', event => console.log(event));
// }
// printOutput(add(5)); // 첫번째 값은 첫번째 매개변수에 할당되기 때문에 default를 받아들이는 매개변수는 처음에 올 수 없음

const hobbies = ['Sports', 'Cooking'];
const activeHobbies = ['Hiking'];

activeHobbies.push(...hobbies); // 스프레드 연산자: 배열을 풀어서 push함

const person = {
    firstName: 'Max',
    age: 30
};
const copiedPerson = {...person};
// copiedPerson = person // 주소값만 참조하고 실제 값이 복사되지 않음

// 나머지 매개변수 
// 인수를 무한정으로 받아들일 수 있음 
const add2 = (...numbers: number[]) => {
    return numbers.reduce((curResult, curValue) => {
        return curResult + curValue;
    }, 0);
}
const addedNumbers = add2(5, 10, 2, 3.7);
console.log(addedNumbers);

// 비구조화 
// 인용하여 추출한 값을 배열에서 삭제하거나 비구조화되지 않고 그저 새로운 상수나 변수에 복사됨
const [hobby1, hobby2, ...remainingHobbies] = hobbies;
console.log(hobbies, hobby1, hobby2);

const {firstName: userName, age} = person;
console.log(userName, age, person);
