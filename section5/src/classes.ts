// OOP
// Object: 코드로 작업하는 구체적인 데이터 구조
// Class: 객체의 청사진. 객체의 형태, 포함하는 데이터, 사용할 메소드를 정의

abstract class Department {    
    // field 
    // private readonly id: string; 
    // private name: string;
    protected employees: string[] = [];
    // 정적 필드
    static fiscalYear = 2022;

    // 생성자 method
    // 클래스 및 클래스에 기반하여 생성된 객체에 연결된 함수
    // 객체가 생성될 때 실행 -> 객체의 초기화 작업에 이용
    constructor(protected readonly id: string, public name: string) {
        // readonly: property 초기화 후 값 변경 불가능

        // this.id = id;
        // this.name = n;
        //console.log(this.fiscalYear) //ERROR: this는 클래스를 기반한 인스턴스를 참조하기 때문에 static 접근 불가
    }

    // 정적 메소드
    static createEmployee (name: string) {
        return {name: name};
    }

    abstract describe (this: Department): void;

    // describe(this: Department) { // this: 항상 Department 클래스에 기반한 인스턴스를 참고
    //     console.log(`Department: (${this.id}): ${this.name}`);
    // }

    addEmployee(employee: string) {
        this.employees.push(employee);
    }

    printEmployeeInformation() {
        console.log(this.employees.length);
        console.log(this.employees);
    }
}

class ITDepartment extends Department {
    describe(this: Department): void {
        throw new Error("Method not implemented.");
    }
    admins: string[];
    constructor(id: string, admins: string[]) {
        super(id, 'IT');
        this.admins = admins;
    }
}

class AccountingDepartment extends Department {
    describe(this: Department): void {
        throw new Error("Method not implemented.");
    }
    private lastReport: string;
    private static instance: AccountingDepartment;

    // 캡슐화
    // getter
    get mostRecentReport() {
        if (this.lastReport) {
            return this.lastReport;
        }
        throw new Error('No report found.');
    }

    //setter 
    set mostRecentReport(value: string) {
        if (!value) {
            throw new Error('Please pass in a valid value!');
        }
        this.addReport(value);
    }

    private constructor(id: string, private reports: string[]) {
        super(id, 'Accounting');
        this.lastReport = reports[0];
    }

    static getInstance() {
        if(AccountingDepartment.instance) {
            return this.instance;
        }
        this.instance = new AccountingDepartment('d2', []);
        return this.instance;
    }

    addEmployee(name: string) {
        if(name === 'Max') {
            return;
        }
        this.employees.push(name); // 자식 클래스에서 접근하려면 접근제어자가 protected여야 함
    }

    addReport(text: string) {
        this.reports.push(text);
    }

    printReports() {
        console.log(this.reports);
    }
}

// 인스턴스화 없이 접근 가능
const employee1 = Department.createEmployee('Max');
console.log(employee1, Department.fiscalYear);

// 객체 생성 
//const accounting = new Department('d1', 'Accounting');
const it = new ITDepartment('d1', ['Max']);

it.addEmployee('Max');
it.addEmployee('Manu');
// accounting.employees[2] = 'Anna'; // 클래스 외부에서 접근하는 방법은 위험하다

console.log(it);
it.describe();
it.printEmployeeInformation();

// const accountingCopy = {name: 'DUMMY', describe: accounting.describe};
// accountingCopy.describe();

//const accounting = new AccountingDepartment('d2', []);
const accounting = AccountingDepartment.getInstance();
const accounting2 = AccountingDepartment.getInstance();
console.log(accounting, accounting2);

accounting.mostRecentReport = 'Year and Report';
accounting.addReport('Someting weng wrong...');
console.log(accounting.mostRecentReport);