// interface: 객체의 구조를 정의
// 이 구조를 가져야만 하는 객체들을 위한 타입 체크용으로 사용
// 서로 다른 클래스 간에 기능을 공유하기 위해 사용

// 추상 클래스와의 차이점
// 인터페이스는 구현 디테일을 갖고 있지 않고 / 추상 클래스는 덮어쓴 부분들과 구체적 구헌 부분의 혼합
interface Named {
    readonly name?: string;
    // readonly: 객체의 초기 설정 후로는 변경 불가능
    // ?: 선택적 프로퍼티
}

interface Greetable extends Named{ 
    //age: number;

    greet(phrase: string): void;
}

class Person implements Greetable {
    name?: string;
    age = 30;

    constructor(n?: string) {
        if (n) {
            this.name = n;
        }
    }

    greet(phrase: string) {
        if (this.name) {
            console.log(phrase + '  ' + this.name);
        } else {
            console.log('Hi!');
        }
    }
}

let user1: Greetable;
user1 = new Person();
// user1.name = 'Max' // ERROR: readonly
user1.greet('Hi there - I am');
console.log(user1);

// 함수 타입 인터페이스
// type AddFn = (a:number, b:number) => number;
interface AddFn { 
    (a: number, b: number): number;
}

let add: AddFn;

add = (n1: number, n2: number) => {
    return n1 + n2;
}