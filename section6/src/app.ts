// 1. Intersection type
type Admin = {
    name: string;
    privileges: string[];
};

type Employee = {
    name: string;
    startDate: Date;
};

type ElevatedEmployee = Admin & Employee;
const e1: ElevatedEmployee = {
    name: 'Sujin',
    privileges: ['create-server'],
    startDate: new Date()
};

type Combinable = string | number;
type Numeric = number | boolean;
type Universal = Combinable & Numeric;

// 2. Type guard: 특정 속성이나 메서드를 사용하기 전에 존재 여부를 및 타입을 확인
// (1) typeof
function add(a: Combinable, b: Combinable) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }
    return a + b;
}
 
// (2) in
type UnKnownEmployee = Employee | Admin;
function printEmployeeInformation(emp: UnKnownEmployee) {
    console.log(emp.name);
    if ('privileges' in emp) {
        console.log('Privileges: ' + emp.privileges);
    }
    if ('startDate' in emp) {
        console.log('startDate: ' + emp.startDate);
    }
}
printEmployeeInformation({name: 'Sujin', startDate: new Date()});

// (3) instanceof
class Car { 
    drive() {
        console.log('Driving...');
    }
}

class Truck {
    drive() {
        console.log('Driving a truck...');
    }
    loadCargo(amount: number) {
        console.log('Loading Cargo...' + amount);
    }
}

type Vehicle = Car | Truck;
const v1 = new Car();
const v2 = new Truck();

function useVehicle(vehicle: Vehicle) {
    vehicle.drive();
    if (vehicle instanceof Truck) {
        vehicle.loadCargo(1000);
    }
}

useVehicle(v1);
useVehicle(v2);

// 3. Discriminated Unions
// 구별 유니온을 구성하는 모든 객체는 객체를 설명하는 하나의 공통 속성을 가진다. 해당 속성을 타입 체크에 사용.
interface Bird { 
    type: 'bird';
    flyingSpeed: number;
}

interface Horse { 
    type: 'horse';
    runningSpeed: number;
}

type Animal = Bird | Horse;
function moveAnimal(animal: Animal) {
    let speed;
    switch (animal.type) {
        case 'bird':
            speed = animal.flyingSpeed;
            break;
        case 'horse':
            speed = animal.runningSpeed;
    }
    console.log('Moving at speed: ' + speed);
}

moveAnimal({type: 'bird', flyingSpeed: 10});

// 4. Type Casting
//const userInputElement = <HTMLInputElement> document.getElementById('userInput')!;
//const userInputElement = document.getElementById('user-input')! as HTMLInputElement;
const userInputElement = document.getElementById('user-input');

if (userInputElement) {
    (userInputElement as HTMLInputElement).value = 'Hi there!';
}

// 5. Index type 
interface ErrorContainer { 
    [prop: string]: string; // 속성의 이름, 개수는 모르지만 해당 객체에 추가되는 속성은 모두 문자열이어야 함
}

const errorBag: ErrorContainer = {
    email: 'Not a valid email!', 
    username: 'Must start with a capital character!'
}

// 6. Function Overloading
// 다양한 매개 변수를 가진 하나의 함수 내에서 무언가 하기 위해 해당 함수를 호출하는 여러 가지 방법을 가진다.
// 자체적으로 반환 값을 정확하게 추론하지 못 할 때 사용.
type CombinableType = string | number;

function add2(a: number, b:number): number;
function add2(a: string, b:string): string;
function add2(a: string, b:number): string;
function add2(a: number, b:string): string;
function add2(a: CombinableType, b: CombinableType) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }
    return a + b;
}

const result = add2(5, 'Sujin');
result.split(' ');

// 7. Optional chaining
// 객체 데이터에 있는 중첩된 속성과 객체에 안전하게 접근하기 위함
// 물음표 앞에 있는 요소가 정의되지 않았다면 해당 요소에 접근하지 않는다.
const fetchedUserData = {
    id: 'u1',
    name: 'Max',
    job: {title: 'CEO', description: 'My own company'}
}
console.log(fetchedUserData?.job?.title);

// 8. Nullish Coalescing 
const userInput = '';

//const storedData = userInput || 'DEFAULT'; // null, undifined, '' 에 모두 해당
const storedData = userInput ?? 'DEFAULT'; // null, undifined에만 한정

console.log(storedData);